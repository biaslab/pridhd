import math

import torch
from qtorch import quant


def rand_from_data(x: torch.Tensor, n_new: int) -> torch.Tensor:
    """
    Creates a tensor of shape `n_new` that roughly shares the same
    distribution as input `x`.
    """

    x = torch.sort(x)[0]  # sort to interpolate in later steps
    new = torch.empty(n_new, dtype=x.dtype, device=x.device)
    n_data = x.size(0)
    for k in range(n_new):
        val = torch.rand([])
        i = int((n_data - 1)*val)  # random index in 0 .. len(data) - 1
        ratio = n_data*val - int(n_data*val)
        new[k] = (1 - ratio)*x[i] + ratio*x[i+1]  # interpolate values
    return new


def quantize(x: torch.Tensor, n_bits: int = 1) -> torch.Tensor:
    """
    Quantize input `x` to have `n_bits` bits.
    """

    if n_bits >= 32:
        return x
    n_bits = n_bits - 1
    exp_man_ratio = 11/64  # same ratio as IEEE 754 double float
    man = int(exp_man_ratio*n_bits)
    exp = n_bits - man
    x = quant.float_quantize(x, exp=exp, man=man, rounding="nearest")
    return x


def cos_cdist(a: torch.Tensor, b: torch.Tensor,
              eps: float = 1e-8) -> torch.Tensor:
    """
    Cosine similarity pairwise matrix of vectors in `a` of size `(n?, d?)`
    with vectors of `b` of size `(m?, d?)`.
    The output is matrix `c` of size `(n, m)` where

    `c[i, j] = a[i, :] · b[j, :] / ||a[i, :]|| ||b[j, :]||

    We avoid dividing by zero by making the divisor greater or equal than
    `eps`.
    """

    norms1 = a.norm(dim=1).unsqueeze_(1)
    norms2 = b.norm(dim=1).unsqueeze_(0)
    den = torch.clamp(norms1*norms2, min=eps)
    cdist = a @ b.T
    cdist = cdist/den
    return cdist


def decode_gd(x: torch.Tensor, y: torch.Tensor, lr: float = 1e-4,
              epochs: int = 200) -> torch.Tensor:
    """
    Computes linear regression without intercept using gradient descent of real
    inputs `x` of size `(n?, m?)` and expected outputs `y` of size `(n?, k?)`
    for `epochs` epochs and `lr` as learning rate.

    Returns coefficient matrix of size `(k, m)`.
    """

    (n, m) = x.size()
    k = y.size(1)
    coef = torch.zeros(k, m, device=x.device, dtype=x.dtype)
    for _ in range(epochs):
        errs = x @ coef.T - y
        delta = errs.T @ x
        coef = coef - lr*delta
    return coef
