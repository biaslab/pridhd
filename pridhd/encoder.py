"""
Linear encoder.

Performs a random linear map with bipolar bases +/- 1.
"""

from __future__ import annotations

import torch


class Encoder(object):
    """
    Initializes a random bipolar encoder.

    `n_in` is the number of features expected in the inputs, `n_out` is the
    dimensionality of encoded (output) data.
    """

    def __init__(self, n_in: int, n_out: int = 8912):
        # random bipolar base +/- 1
        self.basis = torch.where(
            torch.randint(0, 2, size=(n_in, n_out), dtype=torch.bool),
            1.0, -1.0
        )

    def to(self, *args) -> Encoder:
        """
        Changes device or dtype of the underlying representation
        """

        self.basis = self.basis.to(*args)
        return self

    def __call__(self, x: torch.Tensor) -> torch.Tensor:
        """
        Encodes the data `x` to high dimensional space.
        """

        x = x @ self.basis
        return x
