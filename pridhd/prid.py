"""
PIRD model: High dimensional, privacy-preserving classifier.
"""

from __future__ import annotations

from typing import Optional, Union

import torch

from . import utils
from . import Encoder


class PRID(object):
    """
    Creates new PRID classifier, with `n_classes` in the classification
    problem, `n_features` is the dimensionality of the inputs and `n_dim`
    is the desired dimensionality of encoded data.
    """
    def __init__(self, n_classes: int, n_features: int, n_dim: int = 8912):
        self.n_dim = n_dim
        self.n_classes = n_classes
        self.n_features = n_features
        self.encoder = Encoder(n_features, n_out=n_dim)
        self.model = 0.01*torch.randn(n_classes, n_dim)

        self._xdist = None

    def __call__(self, x: torch.Tensor, encoded: bool = False) -> torch.Tensor:
        """
        Returns the pairwise cosine similarity between the class hypervectors
        (model) and the inputs `x` of shape `(n?, n_features)` if not `encoded`
        or `(n?, n_dim)` otherwise.
        If `encoded` is not True, the encoder will be called before doing
        pairwise similarity

        Returns matrix `out` of size `(n, n_classes)` where
        `out[i, k]` is the similarity of input `i` with class hypervector `k`.
        """

        h = x if encoded else self.encode(x)
        out = utils.cos_cdist(h, self.model)
        return out

    def predict(self, x: torch.Tensor, encoded: bool = False) -> torch.Tensor:
        """
        Returns prediction integer matrix of size `(x.shape[0])`

        Equivalent to calling `torch.argmax(pridhd(x, encoded=encoded))`.
        """

        out = self(x, encoded=encoded)
        yhat = torch.argmax(out, 1)
        return yhat

    def encode(self, x: torch.Tensor) -> torch.Tensor:
        """
        Encodes inputs `x` of size `(n?, n_features)` and returns
        the encoded matrix of size `(n, n_dim)`.
        """

        h = self.encoder(x)
        return h

    def decode(self, h: torch.Tensor) -> torch.Tensor:
        """
        Tries to decode matrix `h` of size `(n?, n_dim)` to shape
        `(n, n_features)` using gradient descent regression.

        Note:
            If you intend to use this method to decode the class hypervectors
            or do either of the replacement methods to show leakage, plase call
            :func:`normalize` before decoding.
        """

        x = utils.decode_gd(self.encoder.basis.T, h.T)
        return x

    def noisify(self, ratio: float = 0.05) -> PRID:
        """
        Adds `ratio` of random noise to the class hypervectors
        """

        n_replace = int(ratio*self.n_dim)
        var = torch.var(self.model, 0)
        dim_replace = torch.topk(var, n_replace, largest=False)[1]
        new_vals = utils.rand_from_data(torch.flatten(self.model), n_replace)
        self.model[:, dim_replace] = new_vals
        return self

    def quantize(self, n_bits: int = 1) -> PRID:
        """
        Quantizes class hypervector to the number of bits specified
        """

        self.model = utils.quantize(self.model, n_bits)
        return self

    def fit(self, x: torch.Tensor, y: torch.Tensor, encoded: bool = False,
            lr: float = 0.035, epochs: int = 120, one_pass: bool = True,
            batch_size: Optional[Union[int, float]] = 64
            ) -> PRID:
        """
        Performs training of inputs `x` and expected classes `y` for `epochs`
        iterations and learning rate `lr`, dividing in batches of size
        `batch_size`.

        If `batch_size` is None, no batches will be used (will train over
        the entire dataset at a time), if is floating point value, it will
        train with batches of size `x.shape[0]*batch_size` and if is integer
        it will be used literally as the batch size.

        If not `encoded`, PRIDHD will encode `x` before training.
        If `one_pass`, PRIDHD will perform hyperdimensional one-pass training
        (batch zero): it will initialize the class hypervectors with the
        accumulated vectors of each class, roughly `model[k] = sum(x[y == k])`
        """

        if batch_size is None:
            batch_size = x.size(0)
        if isinstance(batch_size, float):
            batch_size = int(batch_size*x.size()[0])

        if encoded:
            h = x
        else:
            h = self.encode(x)
            self._xdist = torch.max(x) - torch.min(x)

        if one_pass:
            self._one_pass_fit(h, y)
        self._iterative_fit(h, y, lr, epochs, batch_size)
        return self

    def to(self, *args) -> PRID:
        """
        Changes device or dtype of the underlying representation
        """

        self.model = self.model.to(*args)
        self.encoder = self.encoder.to(*args)
        return self

    def normalize(self, xdist=None):
        """
        Normalizes the class hypervectors so that doing
        ``prid.decode(prid.model)`` returns the decoded class hypervectors in
        the correct scale.

        Must be called after training, and if during training the original data
        was not passed (i.e. using ``fit(encoded=True)``) then must specify
        the parameter ``xdist = x_data.max() - x_data.min()``.

        This parameter scales the class hypervectors, following the update
        equation ``model = xdist*model/(x.max() - x.min())`` where
        ``x = decode(model)``.

        Returns self.
        """

        if xdist is None:
            xdist = self._xdist
        if xdist is None:
            raise ValueError(
                "Attempted to normalize before training, or when trained with "
                "`fit(encoded=True)` but no `xdist` specified"
            )

        x = self.decode(self.model)
        modeldist = torch.max(x) - torch.min(x)
        self.model = xdist*self.model/modeldist
        return self

    # single pass training, accumulates all input data of the same
    #  class together and adds it to the class hypervector of that class
    def _one_pass_fit(
            self, h: torch.Tensor, y: torch.Tensor
            ) -> None:

        for lbl in torch.unique(y):
            self.model[lbl] += torch.sum(h[y == lbl], 0)

    # classic iterative training
    def _iterative_fit(
            self, h: torch.Tensor, y: torch.Tensor, lr: float, epochs: int,
            batch_size: int
            ) -> None:

        n = h.size(0)
        for _ in range(epochs):
            for i in range(0, n, batch_size):
                h_batch = h[i:i+batch_size]
                y_batch = y[i:i+batch_size]
                yhat_batch = self.predict(h_batch, encoded=True)
                wrong = y_batch != yhat_batch
                for lbl in y.unique():
                    h1 = h_batch[wrong & (y_batch == lbl)]
                    h2 = h_batch[wrong & (yhat_batch == lbl)]
                    self.model[lbl] += lr*torch.sum(h1, 0)
                    self.model[lbl] -= lr*torch.sum(h2, 0)
