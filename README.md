# pridhd

This repository has the source code for "PRID: Model Inversion Privacy Attacks in Hyperdimensional Learning Systems".

## Install

If you wish to use PRID in your own projects, you can install this module using
```sh
pip install git+https://gitlab.com/biaslab/pridhd
```

## Quick Start

This repository contains an example of execution in the `main.py` file.
In order to run this code you might wish to use.

```sh
git clone https://gitlab.com/biaslab/pridhd.git
cd pridhd
poetry install
poetry run python main.py
```

Please take a look to the [poetry project](https://python-poetry.org/) for more information.

## Citation Request

If you use pridhd code, please cite the following paper:

- Alejandro Hernández-Cano, Rosario Cammarota, Mohsen Imani "PRID: Model Inversion Privacy Attacks in Hyperdimensional Learning Systems". 2021 58th ACM/IEEE Design Automation Conference (DAC).
