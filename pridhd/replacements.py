import torch

from . import PRID


def dimension_replacement(model: PRID, x: torch.Tensor, y: torch.Tensor,
                          n_its: int = 5) -> torch.Tensor:
    """
    Performs dimension replacement in the class hypervectors of `model`
    in the classification problem given by `(x, y)` for `n_its` iterations.

    Returns the leaked information given by this model.
    """

    dim = model.n_dim
    (n, n_features) = x.size()
    h = model.encode(x)
    h_orig = h.clone()
    idxss = torch.zeros(n, dim, device=x.device, dtype=torch.bool)

    orig_scores = torch.stack([model(spl.unsqueeze(0), encoded=True)[0, lbl]
                               for (spl, lbl) in zip(h, y)])
    eye = torch.eye(dim, device=x.device)
    for it in range(n_its):
        iterator = enumerate(zip(y, h, idxss, orig_scores))
        for (i, (lbl, encoded, idxs, orig_score)) in iterator:
            encoded_alt = encoded.expand(dim, dim)*(1 - eye)
            scores = model(encoded_alt, encoded=True)[:, lbl]
            std = torch.std(scores)
            to_update = scores < orig_score - std
            class_repl = to_update & ~idxs
            feat_repl = to_update & idxs
            encoded[class_repl] = model.model[lbl, class_repl].clone()
            encoded[feat_repl] = h_orig[i, feat_repl].clone()
            idxs[class_repl] = True
            idxs[feat_repl] = False
        h = model.encode(model.decode(h))
    x_rec = model.decode(h)
    return x_rec


def feature_replacement(model: PRID, x: torch.Tensor, y: torch.Tensor,
                        n_its: int = 5) -> torch.Tensor:
    """
    Performs dimension replacement in the class hypervectors of `model`
    in the classification problem given by `(x, y)` for `n_its` iterations.

    Returns the leaked information given by this model.
    Performs dimension replacement in the class hypervectors of `model`
    in the classification problem given by `(x, y)` for `n_its` iterations.

    Returns the leaked information given by this model.
    """

    (n, n_features) = x.size()
    h = model.encode(x)
    x_orig = x.clone()
    x_rec = x.clone()
    class_dec = model.decode(model.model)
    idxss = torch.zeros(n, n_features, device=x.device, dtype=torch.bool)
    eye = torch.eye(n_features, device=x.device, dtype=x.dtype)

    for it in range(n_its):
        iterator = enumerate(zip(x_rec, y, h, idxss))
        for (i, (rec, lbl, encoded, idxs)) in iterator:
            spl_alt = rec.expand(n_features, n_features)*(1 - eye)
            encoded_alt = model.encode(spl_alt)
            scores = model(encoded_alt, encoded=True)[:, lbl]

            std = torch.std(scores)
            orig_score = model(encoded.unsqueeze(0), encoded=True)[0, lbl]
            to_update = scores < orig_score - std
            class_repl = to_update & ~idxs
            feat_repl = to_update & idxs

            rec[class_repl] = class_dec[lbl, class_repl].clone()
            rec[feat_repl] = x_orig[i, feat_repl].clone()

            idxs[class_repl] = True
            idxs[feat_repl] = False

    return x_rec
