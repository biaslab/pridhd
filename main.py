import numpy as np
import torch
import sklearn.datasets
import sklearn.preprocessing
import sklearn.model_selection
from matplotlib import pyplot as plt

import pridhd


def show_classhvs(model: pridhd.PRID) -> None:
    n_classes = model.n_classes
    class_dec = model.decode(model.model)
    for (i, dec) in enumerate(class_dec):
        dec = torch.reshape(dec, (28, 28)).detach().cpu().clone().numpy()
        plt.subplot(1, n_classes, i+1)
        plt.title(f"Class {i}")
        plt.imshow(dec)
    plt.show()


def show_leakage(model: pridhd.PRID, x: torch.Tensor, y: torch.Tensor) -> None:
    # do replacements
    n_its = 8
    x_feat = pridhd.replacements.feature_replacement(model, x, y, n_its=n_its)
    x_dim = pridhd.replacements.dimension_replacement(model, x, y, n_its=n_its)
    x_both = pridhd.replacements.feature_replacement(model, x, y,
                                                     n_its=n_its//2)
    x_both = pridhd.replacements.dimension_replacement(model, x_both, y,
                                                       n_its=n_its//2)

    # prepare plot
    xmin = torch.min(x)
    xmax = torch.max(x)
    x = x.view(-1, 28, 28).cpu().numpy()
    x_feat = x_feat.view(-1, 28, 28).cpu().numpy()
    x_dim = x_dim.view(-1, 28, 28).cpu().numpy()
    x_both = x_both.view(-1, 28, 28).cpu().numpy()

    # plot
    n_cols = x.shape[0]
    n_rows = 4
    plt.suptitle(f"Leakage showcase with {n_its} iters")
    for (i, (orig, feat, dim, both)) in enumerate(zip(x, x_feat, x_dim,
                                                      x_both)):
        plt.subplot(n_rows, n_cols, 0*n_cols + i + 1)
        plt.title(f"Sample #{i}")
        plt.imshow(orig, vmin=xmin, vmax=xmax)
        if i == 0:
            plt.ylabel("Original sample")

        plt.subplot(n_rows, n_cols, 1*n_cols + i + 1)
        plt.imshow(feat, vmin=xmin, vmax=xmax)
        if i == 0:
            plt.ylabel("Feature\nreplacement")

        plt.subplot(n_rows, n_cols, 2*n_cols + i + 1)
        plt.imshow(dim, vmin=xmin, vmax=xmax)
        if i == 0:
            plt.ylabel("Dimension\nreplacement")

        plt.subplot(n_rows, n_cols, 3*n_cols + i + 1)
        plt.imshow(both, vmin=xmin, vmax=xmax)
        if i == 0:
            plt.ylabel("Both types\nof replacement")
    plt.show()


def main():
    print("Loading...")
    (x, y) = sklearn.datasets.fetch_openml("mnist_784", return_X_y=True,
                                           as_frame=False)
    y = sklearn.preprocessing.LabelEncoder().fit_transform(y)
    (x, x_test, y, y_test) = sklearn.model_selection.train_test_split(x, y)
    x = torch.from_numpy(x).float()
    x_test = torch.from_numpy(x_test).float()
    y = torch.from_numpy(y).long()
    y_test = torch.from_numpy(y_test).long()
    n_features = x.size(1)
    n_classes = torch.unique(y).size(0)

    model = pridhd.PRID(n_classes, n_features, n_dim=2048)
    if torch.cuda.is_available():
        print("Using cuda!")
        model = model.to("cuda")
        x = x.to("cuda")
        x_test = x_test.to("cuda")
        y = y.to("cuda")
        y_test = y_test.to("cuda")

    print("Encoding...")
    h = model.encode(x)
    h_test = model.encode(x_test)

    print("Fitting...")
    model.fit(h, y, epochs=0, encoded=True)  # one pass fitness
    model.normalize(xdist=torch.max(x) - torch.min(x))

    print("Inference...")
    yhat_test = model.predict(h_test, encoded=True)
    print(f"Accuracy = {torch.sum(y_test == yhat_test)/y_test.size(0):.6f}")

    print("Decoding class hyepervectors...")
    show_classhvs(model)

    print("Leaking info...")
    n_leak = 8
    x_use = x_test[:n_leak, :]
    y_use = y_test[:n_leak]
    show_leakage(model, x_use, y_use)

    print("Secure training...")
    epochs = 5
    for epoch in range(epochs):
        print(f"({epoch + 1}/{epochs})...", end=" ", flush=True)
        model = model.noisify(ratio=0.2)
        model = model.quantize(n_bits=4)
        model = model.fit(h, y, epochs=1, encoded=True, one_pass=False)
    model.normalize(xdist=torch.max(x) - torch.min(x))
    print()

    print("Inference...")
    yhat_test = model.predict(h_test, encoded=True)
    print(f"Accuracy = {torch.sum(y_test == yhat_test)/y_test.size(0):.6f}")

    print("Decoding class hyepervectors...")
    show_classhvs(model)

    print("Leaking info...")
    show_leakage(model, x_use, y_use)


if __name__ == "__main__":
    main()
